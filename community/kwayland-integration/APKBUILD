# Contributor: Bhushan Shah <bshah@kde.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=kwayland-integration
pkgver=5.23.1
pkgrel=0
pkgdesc="KWayland integration"
url="https://kde.org/plasma-desktop/"
arch="all !armhf" # armhf blocked by extra-cmake-modules
license="LGPL-2.1-only OR LGPL-3.0-only"
depends="kglobalaccel"
makedepends="
	extra-cmake-modules
	kguiaddons-dev
	kidletime-dev
	kwayland-dev
	kwindowsystem-dev
	qt5-qtbase-dev
	wayland-protocols
	"

case "$pkgver" in
	*.90*) _rel=unstable;;
	*) _rel=stable;;
esac
source="https://download.kde.org/$_rel/plasma/$pkgver/kwayland-integration-$pkgver.tar.xz"
options="!check" # Broken

build() {
	cmake -B build \
		-DCMAKE_BUILD_TYPE=None \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
6287004978e61572765d3826d395c643b9e1ee54ae0b82060f0c367663e522abacf8a2e4440c275c39beec836d5c8e5aaa175dab5fc4b77c97dc1051a82167da  kwayland-integration-5.23.1.tar.xz
"
