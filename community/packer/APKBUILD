# Maintainer: Galen Abell <galen@galenabell.com>
# Contributor: Galen Abell <galen@galenabell.com>
pkgname=packer
pkgver=1.7.7
pkgrel=0
pkgdesc="tool for creating machine images for multiple platforms"
url="https://www.packer.io/"
license="MPL-2.0"
arch="all"
makedepends="go"
options="net chmod-clean"
source="$pkgname-$pkgver.tar.gz::https://github.com/hashicorp/packer/archive/v$pkgver.tar.gz"

export GOPATH="$srcdir"

build() {
	export GOFLAGS="$GOFLAGS -trimpath"
	export GOLDFLAGS="-s -w -X main.GitCommit=v$pkgver"
	go build -v -o bin/$pkgname
}

check() {
	go list . | xargs -t -n4 go test -timeout=2m -parallel=4
	bin/$pkgname -v
}

package() {
	install -Dm755 bin/"$pkgname" -t "$pkgdir"/usr/bin/
}

sha512sums="
7453371a36ccdee70e5a1c4771f3e9f94aa459a4bdd0b4277bae2eb2d82564fe3c64fde2ac4727117f95f584169a975f0965f3dce4ccc5a4de38ea1928af2271  packer-1.7.7.tar.gz
"
