# Contributor: Jakub Jirutka <jakub@jirutka.cz>
# Maintainer: Jakub Jirutka <jakub@jirutka.cz>
pkgname=starship
pkgver=0.58.0
pkgrel=0
pkgdesc="The minimal, blazing-fast, and infinitely customizable prompt for any shell!"
url="https://starship.rs"
arch="x86_64 armv7 armhf aarch64 x86 ppc64le"  # limited by rust/cargo
license="ISC"
makedepends="cargo libgit2-dev openssl1.1-compat-dev zlib-dev"
source="https://github.com/starship/starship/archive/v$pkgver/$pkgname-$pkgver.tar.gz
	minimize-size.patch
	"
builddir="$srcdir/$pkgname-$pkgver"

# http feature is used just for reporting bugs
_cargo_opts="--locked --no-default-features"

build() {
	cargo build --release $_cargo_opts
}

check() {
	# Some tests sporadically fail, try to repeat 3 times before failing.
	local i; for i in $(seq 0 3); do
		[ $i -eq 0 ] || msg "Retrying ($i/3)..."
		cargo test $_cargo_opts && return 0
		sleep 1
	done
	return 1
}

package() {
	cargo install $_cargo_opts --path . --root="$pkgdir/usr"
	rm "$pkgdir"/usr/.crates*
}

sha512sums="
f04a764434b06a533957c9f335e8b3934bd5d18fadd253ea27f59cf2dc279310b245770c2f03e973b2482e68475ec40f1d0864c691226ae35bc2a3832f757525  starship-0.58.0.tar.gz
cc987156c4e512521aac3da5a5518543d561643855b72500f2cbf203f0a470cc9e4f699d5a236f2504fab5dbc0587ee860d954c2e39239db9a86008dd535ebfb  minimize-size.patch
"
