# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=kwayland-server
pkgver=5.23.1
pkgrel=0
pkgdesc="Wayland Server Components built on KDE Frameworks"
arch="all !armhf" # armhf blocked by qt5-qtdeclarative
url="https://kde.org/plasma-desktop/"
license="GPL-2.0-or-later AND (GPL-2.0-only OR GPL-3.0-only) AND LGPL-2.1-only"
depends_dev="
	kwayland-dev
	plasma-wayland-protocols
	qt5-qtbase-dev
	"
makedepends="$depends_dev
	doxygen
	extra-cmake-modules
	linux-headers
	qt5-qttools-dev
	wayland-protocols
	"

case "$pkgver" in
	*.90*) _rel=unstable;;
	*) _rel=stable;;
esac
source="https://download.kde.org/$_rel/plasma/$pkgver/kwayland-server-$pkgver.tar.xz"
subpackages="$pkgname-dev $pkgname-dbg"
options="!check" # Requires running wayland compositor

build() {
	cmake -B build \
		-DCMAKE_BUILD_TYPE=None \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_QCH=ON
	cmake --build build
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}
sha512sums="
a060d10f34c83bdbbbe26b007f166f8b10a314d572ef65f801a3191e4685964e0b48a89d9e1df5600cc766c686d9f9eac01c0b7e5178856ced5f8fd9ba5c72de  kwayland-server-5.23.1.tar.xz
"
